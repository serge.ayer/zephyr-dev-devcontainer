#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
chmod +x "$DIR/../bin/STM32TrustedPackageCreator"
chmod +x "$DIR/../bin/STM32TrustedPackageCreator_CLI"
chmod +x "$DIR/../bin/Utilities/Linux/imgtool"
chmod +x "$DIR/../bin/Utilities/Linux/PSA_ADAC"

#!/usr/bin/python3

# Strongly inspired from: https://github.com/pbelskiy/docker-usb-sync

import syslog
import subprocess
import os
import glob


def log(message):
    print(message)
    syslog.syslog(message)


def exec_in_container(cmd, name="devcontainer-zephyr-1"):
    output = ""
    try:
        output = subprocess.check_output(
            "docker exec {} bash -c '{}'".format(name, cmd),
            universal_newlines=True,
            shell=True
        )
    except subprocess.CalledProcessError:
        pass

    return output



root_dev = "/dev/bus/usb"

for bus_num in os.listdir(root_dev):
    bus_path = os.path.join(root_dev, bus_num)

    container_devices = exec_in_container("ls " + bus_path).split("\n")
    host_devices = os.listdir(bus_path)

    # remove dead links
    for device in container_devices:
        if device and device not in host_devices:
            print(f"rm: {bus_path}/{device}")
            exec_in_container("rm " + os.path.join(bus_path, device))

    # add new links
    for device in host_devices:
        if device not in container_devices:
            stat = os.stat(os.path.join(bus_path, device))

            cmd = "mknod {device} c {major} {minor}".format(
                device=os.path.join(bus_path, device),
                major=os.major(stat.st_rdev),
                minor=os.minor(stat.st_rdev)
            )
            print(f"cmd: {cmd}")
            exec_in_container(cmd)


# Create ttyACM devices
tty_acm = "/dev/ttyACM"

host_ttys = glob.glob(tty_acm+"*")
container_ttys = exec_in_container("ls " + tty_acm + "*").split("\n")
#print(host_ttys)
#print(container_ttys)

# remove dead links
for container_tty in container_ttys:
    if container_tty and container_tty not in host_ttys:
        print(f"rm: {container_tty}")
        exec_in_container("rm " + os.path.abspath(container_tty))

for host_tty in host_ttys:
    if host_tty and host_tty not in container_ttys:
        stat = os.stat(os.path.abspath(host_tty))
        cmd = "mknod {device} c {major} {minor}".format(
            device=os.path.abspath(host_tty),
            major=os.major(stat.st_rdev),
            minor=os.minor(stat.st_rdev)
        )
        print(f"cmd: {cmd}")
        exec_in_container(cmd)
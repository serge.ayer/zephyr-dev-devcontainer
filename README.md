# Development environment for teaching Zephyr at HEIA-FR/MSE
The development environment is based on a devcontainer (VScode with docker) providing a common platform between Windows and Linux, as well as a stable and documented environment.

The following figure illustrates the development environment that can be used. Instructions for setting up this development environment are given below

![image](docs/Architecture.png)

## Creation of the docker image

The devcontainer is based on the Dockerfile contained in this project. To build this image, the following commands must be used:
> :warning: **If the password contains special characters**: it is preferable to use a gitlab [authentication token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

```bash
docker build -f .devcontainer/Dockerfile -t zephyr-teaching/dev-container . --progress=plain --no-cache
```

## Starting the devcontainer
To start VScode in the docker container, you first need to install the extension `ms-vscode-remote.remote-containers`.

Then simply use the command `crl+shift+p` and choose `Dev containers: Reopen in Container`.


## Development process

Once VScode has been started in the docker container, it is possible to open a development environment:
```bash
code /workdir/my-workspace/ble-course
```

To compile the application:
```bash
west build -b TODO .
```

To flash  the board:
```bash
west flash
```

## Flashing the board with a machine running Windows 

For Windows machines, an additional step is required to flash the boards. This step enables a usb port to be shared between Windows and WSL2.

This sharing is made possible by the `usbipd` application (Download the `.msi` file [here](https://github.com/dorssel/usbipd-win/releases/tag/v4.1.0)).


To determine which device to attach to WSL2, the following command can be used inside `cmd`: 
```bash
usbipd list
```
Before attaching a device to WSL2, it must be shared. This can be done with the following command. This command must be run in `cmd` in administrator mode.
```bash
usbipd bind --busid <busid>
```

Attaching a usb port to WSL2 is done using the following command. An instance of WSL2 must be running for this command to work.
```bash
usbipd attach --wsl --busid <busid>
```

Finally, use the following command inside WSL2, to check that the usb device is available.
```bash
lsusb
```

## Syncing ttyACM device
If an embedded system is disconnected from the host, or if a new embedded system is connected to the host, the corresponding ttyACM file must be created.

The "docker-usb-sync.py" file does this. It also removes dead links.

This file is used as follows:
```bash
python3 ./docker-usb-sync.py
```
Or directly as follows, if the file is executable:
```bash
./docker-usb-sync.py
```
